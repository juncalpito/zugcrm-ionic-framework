// import { HttpClient } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { ProfileServiceProvider } from '../profile-service/profile-service';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/operator/takeWhile"; 
import { Observable } from 'rxjs/Rx';


@Injectable()
export class RequestAgentServiceProvider {

  // getApiUrl : string = "https://jsonplaceholder.typicode.com/posts";
  requestAgentUrl: string = "/get_agents";
  checkForResponseUrl: string = "/check_response_agent";
  updatePositionUrl: string = "/update_agent_position";
  sendAgentMessageUrl: string = "/send_agent_message";
  testApiUrl: string = "/test_api";
  latitude : any;
  longitude : any;
  contactId : any;
  agents : any = [];
  count : any = 0;
  accepted : boolean = false;
  agentAccepted : any;
  agentId : any;
  agentLatitude : any;
  agentLongitude : any;

  constructor(public http: Http, public profileService: ProfileServiceProvider) {
    console.log('Hello RequestAgentServiceProvider  Provider');
  }

  initMap() {
    
  }

  getPosts() {

    // return this.http.get('http://swapi.co/api/films').map(res => res.json());

    // return this.http.get("https://jsonplaceholder.typicode.com/posts")
    //         .map(res => res.json())
    //         .catch(this.handleError);

    this.http.get(this.requestAgentUrl).subscribe(data => {
      console.log((<any>data)._body);
    });
  }

  testApi() {
    return new Promise((resolve, reject) => {
      let alive: boolean = true;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let myData = JSON.stringify({
        latitude: 'Test Data Sent'
      });

      this.http.post(this.testApiUrl, myData, { headers: headers })
        .takeWhile(() => alive)
        .subscribe(data => {
          let temp = JSON.parse(data["_body"]);
          console.log(temp);
          resolve(temp);
        }, error => {
          reject("Oooops!\n" + error);
        });


    });
  }

  requestAgent() {

    return new Promise((resolve, reject) => {
      let alive:boolean = true;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let myData = JSON.stringify({
        latitude: this.latitude,
        longitude: this.longitude,
        first_name: this.profileService.firstName,
        last_name: this.profileService.lastName,
        email_address: this.profileService.email,
        phone: this.profileService.phoneNumber
      });

      this.http.post(this.requestAgentUrl, myData, {headers:headers})
      .takeWhile(() => alive)
      .subscribe(data => {
        let temp = JSON.parse(data["_body"]);
        this.count = temp["count"];
        this.contactId = temp["id"];
        this.agents = temp["agents"];
        console.log(temp);
        resolve(this.agents);
      }, error => {
        reject("Oooops!\n"+error);
      });

      
    });
  }

  listenForAccept() {
    
    return new Promise((resolve, reject) => {
      
      let alive: boolean = true;
    
      Observable
          .interval(3000)
          .takeWhile(() => alive)
          .subscribe(x => {
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');

            let myData = JSON.stringify({
              contact_id: this.contactId
            });

            this.http.post(this.checkForResponseUrl, myData, { headers: headers })
              .takeWhile(() => alive)
              .subscribe(data => {
                let temp = JSON.parse(data['_body']);
                console.log(temp);
                if (temp['accepted'] == true) {
                  console.log('accepted');

                  this.accepted = true;
                  this.agentAccepted = temp['agent'];
                  this.agentId = temp['agent_id'];
                  let values = { 'lat': temp['lat'], 'lon': temp['lon'] };
                  resolve(values);
                  alive = false;
                }
              }, error => {
                reject("Oooops!\n" + error);
              });
          });
    });
  }

  sendMessage(message: string) {

    return new Promise((resolve, reject) => {
      let alive: boolean = true;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      let myData = JSON.stringify({
        user_first_name: this.profileService.firstName,
        user_last_name: this.profileService.lastName,
        user_email: this.profileService.email,
        user_phone: this.profileService.phoneNumber,
        agent_email: this.agentAccepted['email'],
        message: message
      });

      this.http.post(this.sendAgentMessageUrl, myData, { headers: headers })
        .takeWhile(() => alive)
        .subscribe(data => {
          // let temp = JSON.parse(data["_body"]);
          console.log(data["_body"]);
          resolve(true);
        }, error => {
          reject("Oooops!\n" + error);
      });
    });

  }


}



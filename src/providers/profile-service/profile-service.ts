import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class ProfileServiceProvider {

	firstName: any;
	lastName: any;
	phoneNumber: any;
	email: any;
	allSet: boolean = false;

	constructor(public http: HttpClient, public storage: Storage) {

	}

	loadProfile() {

		return new Promise((resolve, reject) => {

			this.storage.get('firstName').then((val) => {
				this.firstName = val;
			});
			this.storage.get('lastName').then((val) => {
				this.lastName = val;
			});
			this.storage.get('email').then((val) => {
				this.email = val;
			});
			this.storage.get('phoneNumber').then((val) => {
				this.phoneNumber = val;
			});
			
			resolve(true);

		});

	}

	saveProfile() {
		this.storage.set('firstName', this.firstName);
		this.storage.set('lastName', this.lastName);
		this.storage.set('phoneNumber', this.phoneNumber);
		this.storage.set('email', this.email);
	}

}

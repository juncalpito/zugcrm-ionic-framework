import { Injectable } from '@angular/core';
import { ToastController, AlertController } from 'ionic-angular';

@Injectable()
export class GlobalFunctionsProvider {

	constructor(public toastCtrl: ToastController, public alertCtrl: AlertController) {

	}

	showToast(position: string, message: string) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 2000,
			position: position,
			cssClass: 'text-center'
		});

		toast.present(toast);
	}

	showAlert(title: string, subtitle: string) {
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: subtitle,
			buttons: ['OK']
		});
		alert.present();
	}

	showToastWithCloseButton(position: string, message: string) {
		const toast = this.toastCtrl.create({
			message: message,
			showCloseButton: true,
			position: position,
			closeButtonText: 'Ok'
		});
		toast.present();
	}

	showConfirm(title: string, message: string) {
		let confirm = this.alertCtrl.create({
			title: title,
			message: message,
			buttons: [
				{
					text: 'Ok',
					handler: () => {
						console.log('Ok clicked');
					}
				},
				{
					text: 'Cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				}
			]
		});
		confirm.present();
	}

	checkNumber(number) {

		number.replace(/\D/g, '');
		if (number !== "")
			if (number.length == 10) {
				number = "+1" + number;
			} else {
				number = "+" + number;
			}

		return number;

	}
}

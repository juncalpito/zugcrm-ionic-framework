import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { RequestAgentServiceProvider } from './../../providers/request-agent-service/request-agent-service';
import { GlobalFunctionsProvider } from './../../providers/global-functions/global-functions';

@Component({      
  selector: 'page-email-modal',
  templateUrl: 'email-modal.html',
})
export class EmailModalPage {

  emailMessage: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public requestService: RequestAgentServiceProvider, public globals: GlobalFunctionsProvider, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    
  }

  /**
   * Function to send message to agent who accepted
   * 
   * @memberof EmailModalPage
   */
  sendMessage() {
    this.requestService.sendMessage(this.emailMessage).then(
      (val) => {
        this.viewCtrl.dismiss();
        this.globals.showToast("middle", "The email has been sent!");
      },
      (err) => console.error(err)
    );
  }

}

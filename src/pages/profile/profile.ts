import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProfileServiceProvider } from '../../providers/profile-service/profile-service';
import { RequestAgentPage } from '../request-agent/request-agent';
import { GlobalFunctionsProvider } from './../../providers/global-functions/global-functions';
import { EditProfilePage } from '../edit-profile/edit-profile';


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  
  /**
   * Creates an instance of ProfilePage.
   * @param {NavController} navCtrl 
   * @param {NavParams} navParams 
   * @param {GlobalFunctionsProvider} globals 
   * @param {ProfileServiceProvider} profileService 
   * @memberof ProfilePage
   */
  constructor(public navCtrl: NavController, public navParams: NavParams, public globals: GlobalFunctionsProvider, public profileService: ProfileServiceProvider) {
    this.profileService.loadProfile().then(
      (val) => {
        if (this.profileService.firstName == null || this.profileService.lastName == null || this.profileService.phoneNumber == null || this.profileService.email == null) {
          this.navCtrl.setRoot(EditProfilePage);
          this.profileService.allSet = true;
          console.log('Profile not set');
        }
      },
      (err) => console.error(err)
    );
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  editProfile() {
    this.navCtrl.push(EditProfilePage);
  }

  goToRequest() {
    this.navCtrl.setRoot(RequestAgentPage);
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestAgentPage } from './request-agent';

@NgModule({
  declarations: [
    RequestAgentPage,
  ],
  imports: [
    IonicPageModule.forChild(RequestAgentPage),
  ],
})
export class RequestAgentPageModule {}

import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { RequestAgentDetailsPage } from './../request-agent-details/request-agent-details';
import { EmailModalPage } from './../email-modal/email-modal';
import { RequestAgentServiceProvider } from './../../providers/request-agent-service/request-agent-service';
import { GlobalFunctionsProvider } from './../../providers/global-functions/global-functions';
import { Geolocation } from '@ionic-native/geolocation';
import "rxjs/add/operator/takeWhile";

declare var google;

@IonicPage()
@Component({
	selector: 'request-agent',
	templateUrl: 'request-agent.html',
})
export class RequestAgentPage {

	
	@ViewChild('map') mapElement: ElementRef;
	map: any;
	postList: any[];
	latitude: any;
	longitude: any;
	latLng: any;
	gotLocation = false;
	loading: boolean = false;
	markers: any = [];
	private alive: boolean = true;

	/**
	 * Creates an instance of RequestAgentPage.
	 * @param {NavController} navCtrl 
	 * @param {NavParams} navParams 
	 * @param {Geolocation} geolocation 
	 * @param {ModalController} modalCtrl 
	 * @param {RequestAgentServiceProvider} requestService 
	 * @param {GlobalFunctionsProvider} globals 
	 * @memberof RequestAgentPage
	 */
	constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation, public modalCtrl: ModalController, public requestService: RequestAgentServiceProvider, public globals: GlobalFunctionsProvider) {
		this.requestService.count = 0;
	}

	/**
	 * Default on load function
	 * 
	 * @memberof RequestAgentPage
	 */
	ionViewDidLoad() {
		
	}

	/**
	 * Spinner function
	 * 
	 * @memberof RequestAgentPage
	 */
	showSpinner() {
		this.loading = !this.loading;
	}

	/**
	 * Function to load the Map
	 * 
	 * @memberof RequestAgentPage
	 */
	loadMap() {
		this.loading = true;
		console.log("loading map");

		
		let posOptions = {
			enableHighAccuracy: false,
			timeout: 30000,
			maximumAge: 0
		};

		this.geolocation.getCurrentPosition(posOptions).then((position) => {
			this.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

			this.requestService.latitude = position.coords.latitude;
			this.requestService.longitude = position.coords.longitude;
			// this.gotLocation = true;
			// this.alive = false;
			// this.loading = false;

			let mapOptions = {
				center: this.latLng,
				zoom: 16,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}

			this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

			this.gotLocation = true;
			// this.alive = false;
			this.addMarker("You are here!", this.latLng);
			this.loading = false;
		}).catch((error) => {
			console.log (error.message+ " " +error.code);
			let errMessage: string;

			switch(error.code) {
				case error.PERMISSION_DENIED:
					errMessage = "User denied the request for Geolocation."
				break; 
				case error.POSITION_UNAVAILABLE:
					errMessage = "Location information is unavailable."
				break;
				case error.TIMEOUT:
					errMessage = "The request to get user location timed out."
				break;
				case error.UNKNOWN_ERROR:
					errMessage = "An unknown error occurred."
				break;
			}

			this.globals.showToastWithCloseButton('bottom', 'Error: ' + errMessage);
			console.log(errMessage);
		});

		// let watch = this.geolocation.watchPosition();
		// watch.takeWhile(() => this.alive)
		// .subscribe((data) => {
		// 	this.gotLocation = true;
		// 	this.alive = false;
		// 	this.addMarker("You are here!", this.latLng);
		// 	this.loading = false;
		// });

	}

	/**
	 * Function to add a marker to the map
	 * 
	 * @param {any} message 
	 * @param {any} position 
	 * @memberof RequestAgentPage
	 */
	addMarker(message, position) {

		if (!position) 
		position = this.map.getCenter(); 

		let marker = new google.maps.Marker({
			map: this.map,
			animation: google.maps.Animation.DROP,
			position: position
		});

		let content = "<h4>"+message+"!</h4>";         

		this.addInfoWindow(marker, content);
	}

	/**
	 * Function to add a marker with cab icon
	 * 
	 * @param {any} message 
	 * @param {any} position 
	 * @memberof RequestAgentPage
	 */
	addAgentMarker(message, position) {

		if (!position)
			position = this.map.getCenter();

		let marker = new google.maps.Marker({
			icon: 'https://maps.google.com/mapfiles/ms/micons/cabs.png',
			map: this.map,
			animation: google.maps.Animation.DROP,
			position: position
		});

		this.markers.push(marker);

		let content = "<h8>" + message + "</h8>";

		this.addInfoWindow(marker, content);
	}

	/**
	 * Function to add info popup window to a marker
	 * 
	 * @param {any} marker 
	 * @param {any} content 
	 * @memberof RequestAgentPage
	 */
	addInfoWindow(marker, content) {
		let infoWindow = new google.maps.InfoWindow({
			content: content
		});

		google.maps.event.addListener(marker, 'click', () => {
			infoWindow.open(this.map, marker);
		});
	}

	/**
	 * Function to show the agent who accepted's details
	 * Triggers on 'Show Agent Details' button click
	 * 
	 * @memberof RequestAgentPage
	 */
	showAgentDetails() {
		this.navCtrl.push(RequestAgentDetailsPage);
	}

	/**
	 * Function to show 
	 * Triggers on email button click inside RequestAgentDetailsPage
	 * 
	 * @memberof RequestAgentPage
	 */
	showEmailModal() {
		let emailModal = this.modalCtrl.create(EmailModalPage);
		emailModal.present();
	}

	/**
	 * Function to request an agent
	 * Triggers on 'Request Agent' button click
	 * 
	 * @memberof RequestAgentPage
	 */
	requestAgent() {

		this.loading = true;

		this.requestService.requestAgent().then(
			(val) => {
				console.log(this.requestService.count);
				if (this.requestService.count == 0)
					this.globals.showToast('middle', 'No agents were found near you.');
				else {
					this.globals.showToast('middle', this.requestService.count + ' agent/s found near you. Waiting for agent to accept the request.');
					this.addAllAgentMarkers(val);
					this.requestService.listenForAccept().then(
						(val) => {
							this.removeAgentMarkers();
							var acceptLocation = new google.maps.LatLng(val['lat'], val['lon']);
							this.addAgentMarker('Accepted Agent', acceptLocation);
							this.globals.showToastWithCloseButton('bottom', 'An agent has accepted your request. You can view the agent\'s details and position.');
						},
						(err) => console.log(err)
					);
				}
				this.loading = false;
			},
			(err) => console.error(err) 
		);

	}

	addAllAgentMarkers(agents) {
		console.log(agents);
		agents.forEach((item, index) => {
			// console.log(item); 
			// console.log(index); 
			let position = new google.maps.LatLng(parseFloat(item.last_latitude), parseFloat(item.last_longitude));
			this.addAgentMarker("Agent " + (index+1),position);
		});
	}

	setMapBounds() {
		var bounds = new google.maps.LatLngBounds();
		for (var i = 0; i < this.markers.length; i++) {
			bounds.extend(this.markers[i].getPosition());
		}
		this.map.fitBounds(bounds);
	}

	removeAgentMarkers() {
		for (var i = 0; i < this.markers.length; i++) {
			this.markers[i].setMap(null);
		}
	}

	testApi() {
		console.log("start test");
		this.requestService.testApi().then(
			(val) => {
				this.globals.showToastWithCloseButton('bottom', 'Returned: ' + val);
			}
		)
	}
	
}

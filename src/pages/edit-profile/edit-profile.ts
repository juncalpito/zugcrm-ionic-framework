import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ProfileServiceProvider } from '../../providers/profile-service/profile-service';
import { RequestAgentPage } from '../request-agent/request-agent';
import { GlobalFunctionsProvider } from './../../providers/global-functions/global-functions';

@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public profileService: ProfileServiceProvider, public globals: GlobalFunctionsProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }

  saveProfile() {
    this.profileService.saveProfile();
    this.globals.showToast("bottom", "Profile successfully saved!");
  }

  goToRequest() {
    this.navCtrl.setRoot(RequestAgentPage);
  }

}

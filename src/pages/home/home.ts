import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProfileServiceProvider } from '../../providers/profile-service/profile-service';
import { ProfilePage } from '../profile/profile';
// import { RequestAgentPage } from '../request-agent/request-agent';
import { EmailModalPage } from '../email-modal/email-modal';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	// Slides content used for the home slider
	slides = [
		{
			title: "Welcome to the Zugent Personal App!",
			description: "The <b>Zugent Personal App</b> showcases a number of useful components for Real Estate Customers.",
			image: "../../assets/imgs/zugenthome.png",
		},
		{
			title: "What is Zugent?",
			description: "Zugent is a company dedicated to making life easier by providing you with all your real estate needs.",
			image: "../../assets/imgs/zugenthome.png",
		},
		{
			title: "Picture of Zugent Features?",
			description: "1.  description 1<br>2.  description 2<br>3.  description 3<br>4.  description 4",
			image: "../../assets/imgs/zugenthome.png",
		}
	];

	/**
	 * Creates an instance of HomePage.
	 * @param {NavController} navCtrl 
	 * @param {ProfileServiceProvider} profileService 
	 * @memberof HomePage
	 */
	constructor(public navCtrl: NavController, public profileService: ProfileServiceProvider) {
		
		// Check if data has been set
		if (profileService.email !== "" && profileService.phoneNumber !== "" && profileService.firstName !== "" && profileService.lastName !== "")
			this.navCtrl.setRoot(ProfilePage);
		// this.navCtrl.setRoot(EmailModalPage);
	}

	/**
	 * Function to go to Profile page
	 * 
	 * @memberof HomePage
	 */
	goToProfile() {
		this.navCtrl.setRoot(ProfilePage);
	}

}
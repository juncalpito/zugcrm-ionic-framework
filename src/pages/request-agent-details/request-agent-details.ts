import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EmailModalPage } from './../email-modal/email-modal';
import { RequestAgentServiceProvider } from './../../providers/request-agent-service/request-agent-service';

@IonicPage()
@Component({
  selector: 'page-request-agent-details',
  templateUrl: 'request-agent-details.html',
})
export class RequestAgentDetailsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public requestService: RequestAgentServiceProvider) {
    console.log(this.requestService.agentAccepted);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RequestAgentDetailsPage');
  }

  showEmailModal() {
    this.navCtrl.push(EmailModalPage);
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestAgentDetailsPage } from './request-agent-details';

@NgModule({
  declarations: [
    RequestAgentDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(RequestAgentDetailsPage),
  ],
})
export class RequestAgentDetailsPageModule {}

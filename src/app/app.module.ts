import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, Injectable, Injector, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { RequestAgentPage } from '../pages/request-agent/request-agent'
import { RequestAgentDetailsPage } from '../pages/request-agent-details/request-agent-details'
import { ListingsPage } from '../pages/listings/listings'
import { ProfilePage } from '../pages/profile/profile'
import { EmailModalPage } from '../pages/email-modal/email-modal'
import { EditProfilePage } from '../pages/edit-profile/edit-profile'

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';

import { Pro } from '@ionic/pro';
import { RequestAgentServiceProvider } from '../providers/request-agent-service/request-agent-service';
import { ProfileServiceProvider } from '../providers/profile-service/profile-service';
import { GlobalFunctionsProvider } from '../providers/global-functions/global-functions';

const IonicPro = Pro.init('98af1568', {
  appVersion: "0.1.2"
});

@Injectable()
export class MyErrorHandler implements ErrorHandler {
  ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
    try {
      this.ionicErrorHandler = injector.get(IonicErrorHandler);
    } catch(e) {
      // Unable to get the IonicErrorHandler provider, ensure 
      // IonicErrorHandler has been added to the providers list below
    }
  }

  handleError(err: any): void {
    IonicPro.monitoring.handleNewError(err);
    // Remove this if you want to disable Ionic's auto exception handling
    // in development mode.
    this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    RequestAgentPage,
    RequestAgentDetailsPage,
    ListingsPage,
    ProfilePage,
    EmailModalPage,
    EditProfilePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    RequestAgentPage,
    RequestAgentDetailsPage,
    ListingsPage, 
    ProfilePage,
    EmailModalPage,
    EditProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    IonicErrorHandler,
    [{ provide: ErrorHandler, useClass: MyErrorHandler }],
    RequestAgentServiceProvider,
    ProfileServiceProvider,
    GlobalFunctionsProvider
    // {provide: ErrorHandler, useClass: IonicErrorHandler}

  ]
})
export class AppModule {}

import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, PopoverController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { RequestAgentPage } from '../pages/request-agent/request-agent';
import { ListingsPage } from '../pages/listings/listings';
import { ProfilePage } from '../pages/profile/profile';

import { ProfileServiceProvider } from '../providers/profile-service/profile-service';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // Set the root page here
  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  /**
   * Creates an instance of MyApp.
   * @param {Platform} platform 
   * @param {StatusBar} statusBar 
   * @param {SplashScreen} splashScreen 
   * @param {ProfileServiceProvider} profileService 
   * @memberof MyApp
   */
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public profileService: ProfileServiceProvider) {
    this.initializeApp();

    // Place pages here
    this.pages = [
      { title: 'Introduction', component: HomePage },
      { title: 'Listings', component: ListingsPage },
      { title: 'My Profile', component: ProfilePage },
      { title: 'Request Agent', component: RequestAgentPage }
    ];

    // Load profile immediately so that it can load values into the variables
    this.profileService.loadProfile();

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  /**
   * Function to convert json to array
   * 
   * @param {any} val 
   * @returns 
   * @memberof MyApp
   */
  hack(val) {
    return Array.from(val);
  }
}
